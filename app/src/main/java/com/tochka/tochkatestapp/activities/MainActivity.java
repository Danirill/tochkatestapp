package com.tochka.tochkatestapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.tochka.tochkatestapp.App;
import com.tochka.tochkatestapp.R;
import com.tochka.tochkatestapp.network.users.SearchUsersResponse;
import com.tochka.tochkatestapp.network.users.User;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private SearchView searchView;
    private ListView listView;
    private String currentUsername;
    private int currentPage;
    private ArrayList<User> users;
    private ProgressDialog progress;
    private Button pageUp;
    private Button pageDown;
    private int pagesCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        searchView = findViewById(R.id.searchView);
        pageUp = findViewById(R.id.pageUp);
        pageDown = findViewById(R.id.pageDown);
        currentPage = 1;
        pagesCount = 1;
        currentUsername = "Danirill";
        progress = new ProgressDialog(this);

        githubRequest();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                currentUsername = query;
                currentPage = 1;
                pagesCount = 1;
                showLoading();
                githubRequest();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        pageDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageDown();
            }
        });

        pageUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pageUp();
            }
        });
    }

    private void updateList(){
        CustomAdapter customAdapter = new CustomAdapter(this, users);
        listView.setAdapter(customAdapter);
    }

    private void setPagesCount(int count){
        pagesCount = count;
    }

    private void showLoading(){
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false);
        progress.show();
    }

    private void pageUp(){
        if(currentPage == pagesCount)
            return;
        currentPage++;
        showLoading();
        githubRequest();
    }

    private void pageDown(){
        if(currentPage == 1)
            return;
        currentPage--;
        showLoading();
        githubRequest();
    }

    private void showToast(String text){
        Toast toast = Toast.makeText(getApplicationContext(),
                text, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void githubRequest(){
        App.getApi().getUsers2(currentUsername, currentPage).enqueue(new Callback<SearchUsersResponse>() {
            @Override
            public void onResponse(Call<SearchUsersResponse> call, Response<SearchUsersResponse> response) {
                users = new ArrayList<User>();
                progress.dismiss();
                if(response.code() != 200){
                    showToast(String.valueOf(response.code() + " " + response.message()));
                    return;
                }
                if(response.body() == null)
                    return;
                int a = response.body().getTotal_count() / 30;
                if(response.body().getTotal_count() % 30 > 0)
                    a++;
                if(a == 0){
                    showToast("No matches");
                    return;
                }

                setPagesCount(a);
                User[] items = response.body().getItems();
                users.addAll(Arrays.asList(items));
                updateList();
                progress.dismiss();
            }
            @Override
            public void onFailure(Call<SearchUsersResponse> call, Throwable t) {
                progress.dismiss();
                Log.d("RETROFIT",t.getMessage());
            }
        });
    }

}
