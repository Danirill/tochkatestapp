package com.tochka.tochkatestapp.activities;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tochka.tochkatestapp.R;
import com.tochka.tochkatestapp.network.project.ProjectData;

import java.util.List;

public class RecyclerProjectsAdapter extends RecyclerView.Adapter<RecyclerProjectsAdapter.Project> {
    List<ProjectData> list;
    public RecyclerProjectsAdapter(List<ProjectData> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public Project onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview, viewGroup, false);
        return new Project(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final Project holder, int position) {
        final int a = position;
        holder.projectName.setText(list.get(position).getName());
        holder.projectDescription.setText(list.get(position).getLanguage());
        holder.date.setText(list.get(position).getUpdated_at());
        holder.openProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://github.com/" + list.get(a).getFull_name()));
                v.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Project extends RecyclerView.ViewHolder{
        CardView cardView;
        Button openProject;
        TextView projectName;
        TextView projectDescription;
        TextView date;

        public Project(@NonNull View itemView) {
            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.cv);
            openProject = (Button)itemView.findViewById(R.id.openProject);
            projectName = itemView.findViewById(R.id.project_name);
            projectDescription = itemView.findViewById(R.id.project_description);
            date = itemView.findViewById(R.id.projectdate);
        }
    }
}
