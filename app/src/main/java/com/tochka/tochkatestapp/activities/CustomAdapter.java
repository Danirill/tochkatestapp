package com.tochka.tochkatestapp.activities;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.tochka.tochkatestapp.R;
import com.tochka.tochkatestapp.network.users.User;

import java.util.ArrayList;
class CustomAdapter implements ListAdapter {
    ArrayList<User> arrayList;
    Context context;
    public CustomAdapter(Context context, ArrayList<User> arrayList) {
        this.arrayList=arrayList;
        this.context=context;
    }
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }
    @Override
    public boolean isEnabled(int position) {
        return true;
    }
    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final User user = arrayList.get(position);
        if(convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, UserInfo.class);
                    intent.putExtra("avatar", user.getAvatar_url());
                    intent.putExtra("projects", user.getRepos_url());
                    intent.putExtra("url", user.getUrl());
                    intent.putExtra("username", user.getLogin());
                    context.startActivity(intent);
                }
            });
            TextView username = convertView.findViewById(R.id.username);
            ImageView image = convertView.findViewById(R.id.userAvatar);
            username.setText(user.getLogin());
            Picasso.get()
                    .load(user.getAvatar_url())
                    .error(R.drawable.placeholder)
                    .into(image);
        }
        return convertView;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getViewTypeCount() {
        return arrayList.size();
    }
    @Override
    public boolean isEmpty() {
        return false;
    }
}