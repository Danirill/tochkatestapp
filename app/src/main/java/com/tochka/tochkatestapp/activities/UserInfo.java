package com.tochka.tochkatestapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tochka.tochkatestapp.App;
import com.tochka.tochkatestapp.R;
import com.tochka.tochkatestapp.network.project.ProjectData;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserInfo extends AppCompatActivity {
    private String avatarUrl;
    private String projectsUrl;
    private String userUrl;
    private String username;
    private RecyclerView recyclerView;
    private Button openInBrowser;
    private Button share;
    private Button copyLink;
    private String TAG = "USER_INFO";
    private String userlink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        Intent intent = getIntent();
        avatarUrl = intent.getStringExtra("avatar");
        projectsUrl = intent.getStringExtra("projects");
        username = intent.getStringExtra("username");
        userlink = "https://github.com/" + username;
        userUrl = intent.getStringExtra("url");
        setAvatar();
        recyclerView = (RecyclerView)findViewById(R.id.projects);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        setProjects();
        openInBrowser = (Button)findViewById(R.id.openBrowser);
        share = (Button)findViewById(R.id.share);
        copyLink = (Button)findViewById(R.id.copy_link);

        copyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setClipboard(v.getContext(),userlink);
                showToast("Copied to clipboard");
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Check this GitHub Profile!");
                    String shareMessage = userlink;
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch(Exception e) {
                    Log.e(TAG, "error sharing");
                }
            }
        });


        openInBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(userlink));
                startActivity(intent);
            }
        });
    }


    private void setAvatar(){
        ImageView image = findViewById(R.id.big_avatar);
        Picasso.get()
                .load(avatarUrl)
                .error(R.drawable.placeholder)
                .into(image);
    }

    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }

    private void showToast(String text){
        Toast toast = Toast.makeText(getApplicationContext(),
                text, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void setProjects(){
        App.getApi().getProjects(username).enqueue(new Callback<List<ProjectData>>() {
            @Override
            public void onResponse(Call<List<ProjectData>> call, Response<List<ProjectData>> response) {
                Log.d("RETROFIT", String.valueOf(response.code()));
                if(response.code() != 200){
                    return;
                }
                RecyclerProjectsAdapter adapter = new RecyclerProjectsAdapter(response.body());
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<ProjectData>> call, Throwable t) {
                Log.d("RETROFIT",t.getMessage());
            }
        });
    }
}
