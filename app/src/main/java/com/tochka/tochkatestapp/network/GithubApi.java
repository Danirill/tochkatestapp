package com.tochka.tochkatestapp.network;

import com.tochka.tochkatestapp.network.project.ProjectData;
import com.tochka.tochkatestapp.network.users.SearchUsersResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GithubApi {

    @GET("/search/users")
    Call<SearchUsersResponse> getUsers(@Query("q") String username);
    @GET("/search/users")
    Call<SearchUsersResponse> getUsers2(@Query("q") String username,@Query("page") int page);
    @GET("/users/{username}/repos")
    Call<List<ProjectData>> getProjects(@Path("username") String username);
}
