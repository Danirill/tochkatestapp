package com.tochka.tochkatestapp.network.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchUsersResponse {

    @SerializedName("total_count")
    @Expose
    private int total_count;

    @SerializedName("incomplete_results")
    @Expose
    private boolean incomplete_results;

    @SerializedName("items")
    @Expose
    private User[] items;

    public int getTotal_count() {
        return total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public User[] getItems() {
        return items;
    }
}
